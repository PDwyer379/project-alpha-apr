from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import TasksForm
from django.contrib.auth.decorators import login_required
from .models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TasksForm(request.POST)

        if form.is_valid():
            task = form.save()
            task.save()
            messages.success(request, "Task created sucessfully")

            return redirect("list_projects")
    else:
        form = TasksForm()

    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    show_my_tasks = Task.objects.filter(assignee=request.user)

    context = {
        "show_my_tasks": show_my_tasks,
    }

    return render(request, "tasks/show_my_tasks.html", context)
