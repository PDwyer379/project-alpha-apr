from django.shortcuts import render, get_object_or_404, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm
from django.contrib import messages

# Create your views here.


@login_required
def project_view(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": list_projects,
    }
    return render(request, "projects/project.html", context)


@login_required
def show_project(request, id):
    show_project = get_object_or_404(Project, id=id)
    tasks = show_project.tasks.all()
    context = {
        "show_project": show_project,
        "tasks": tasks,
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)

        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            messages.success(
                request,
                "Project created successfully",
            )
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {"form": form}

    return render(request, "projects/create_project.html", context)
