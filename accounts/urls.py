from .views import login_view, sign_up, user_logout
from django.urls import path

urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", sign_up, name="signup"),
]
