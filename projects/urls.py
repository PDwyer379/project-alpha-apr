from django.urls import path
from .views import project_view, show_project, create_project

urlpatterns = [
    path("", project_view, name="list_projects"),
    path("show_project/<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
